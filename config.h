/* See LICENSE file for copyright and license details. */

/* Adds volume keys support*/
#include <X11/XF86keysym.h>

/* Appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 1;        /* 1 means swallow floating windows by default */
static const unsigned int gappih    = 20;       /* horiz inner gap between windows */
static const unsigned int gappiv    = 10;       /* vert inner gap between windows */
static const unsigned int gappoh    = 30;       /* horiz outer gap between windows and screen edge */
static const unsigned int gappov    = 30;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = {"JetBrainsMono Nerd Font Bandit:size=10", "fontawesome:size=12", "JoyPixels:pixelsize=14:antialias=true:autohint=true"};
static const char col_gray1[]       = "#222222"; /* background color  */
static const char col_gray2[]       = "#3b3a37"; /* inactive window border color */
static const char col_gray3[]       = "#928374"; /* font color */
static const char col_gray4[]       = "#d79921"; /* current tag and current window font color */
static const char col_cyan[]        = "#353434"; /* top bar second color (gray) */
static const char col_light_blue[]  = "#83a598"; /* active window border color */
static const unsigned int baralpha = 0xd3;
static const unsigned int borderalpha = OPAQUE;
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan, col_light_blue    },
};
static const unsigned int alphas[][3]      = {
    /*               fg      bg        border     */
    [SchemeNorm] = { OPAQUE, baralpha, borderalpha },
    [SchemeSel]  = { OPAQUE, baralpha, borderalpha },
 };

/* Tagging */
static const char *tags[] = {"1","2","3","4","5","6","7"};
static const unsigned int ulinepad	= 5; /* horizontal padding between the underline and tag */
static const unsigned int ulinestroke	= 2; /* thickness / height of the underline */
static const unsigned int ulinevoffset	= 0; /* how far above the bottom of the bar the line should appear */
static const int ulineall 		= 0; /* 1 to show underline on all tags, 0 for just the active ones */

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
/* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor */
	{ "Firefox", NULL,     NULL,           1 << 8,    0,          0,          -1,        -1 },
	{ "st-256-color",      NULL,     NULL,           0,         0,          1,           0,        -1 },
	{ NULL,      NULL,     "st",           0,         0,          1,           0,        -1},
    { NULL,      "st-256-color",     NULL,           0,         0,          1,           0,        -1},
    { NULL,      NULL,     "newsboat",     0,         0,          1,           0,        -1 },
	{ NULL,      NULL,     "Event Tester", 0,         0,          0,           1,        -1 }, /* xev */

};

/* Layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 0;    /* 1 means respect size hints in tiled resizals */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"
static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "﬿",      tile },    /* first entry is default */
	{ "",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle }, /* Monocle Layout*/
    { "[@]",      spiral },
	{ "[\\]",     dwindle },
	{ "D[]",      deck },
	{ "TTT",      bstack },
	{ "|M|",      centeredmaster },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },
#define STACKKEYS(MOD,ACTION) \
	{ MOD, XK_j,     ACTION##stack, {.i = INC(+1) } }, \
	{ MOD, XK_k,     ACTION##stack, {.i = INC(-1) } }, \
	{ MOD, XK_v,     ACTION##stack, {.i = 0 } }, \
	/*{ MOD, XK_grave, ACTION##stack, {.i = PREVSEL } }, \
	{ MOD, XK_a,     ACTION##stack, {.i = 1 } }, \
	{ MOD, XK_z,     ACTION##stack, {.i = 2 } }, \
	{ MOD, XK_x,     ACTION##stack, {.i = -1 } }, */

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* Commands variables */
static const char *termcmd[] = { "st", NULL}; /* variable for launchinhg st */

static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_d,      spawn,          SHCMD("dmenu_run -c -l 20 -g 3 -bw 5")},
    { MODKEY,                       XK_Return, spawn,          {.v = termcmd}},
    { MODKEY|ShiftMask,             XK_z,      spawn,          SHCMD("keepassxc")},
    { MODKEY|ShiftMask,             XK_x,      spawn,          SHCMD("librewolf")}, /* launches librewolf one the best browsers */
    { MODKEY|ControlMask|ShiftMask, XK_x,      spawn,          SHCMD("firejail chromium") }, /* launches ungoogled chromium with firejail sandboxing */
	{ MODKEY|ShiftMask,             XK_q,      spawn,          SHCMD("dmenusysact")}, /* run the sysact script */
	{ MODKEY|ShiftMask,             XK_a,      spawn,          SHCMD("emacsclient --create-frame")}, /* opens emacs with the emacs daemon */
	{ MODKEY|ShiftMask,             XK_s,      spawn,          SHCMD("yt -g")}, /* launches youtube dmenu script */
	{ MODKEY,                       XK_z,      spawn,          SHCMD("st -e ncmpcpp")}, /* launches ncmpcpp the music player */
    { MODKEY,                       XK_x,      spawn,          SHCMD("st -e newsboat -r")}, /* launches newsboat rss feed reader */
    { MODKEY,                       XK_a,      spawn,          SHCMD("st -e htop")}, /* launches htop */
    { MODKEY,                       XK_comma,  spawn,          SHCMD("mpc prev")},   /* plays the previous track in playlist */
    { MODKEY,                       XK_period, spawn,          SHCMD("mpc next")},   /* play the next track in playlist */
    { MODKEY,                       XK_p,      spawn,          SHCMD("mpc random")}, /* toggles random music from playlist */
	{ MODKEY,                       XK_w,      spawn,          SHCMD("mpc toggle")}, /* toggles music on and off */
	{ MODKEY,                       XK_space,  zoom,           {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
    { MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} }, /* toggles true full screen */
    { 0,                            XK_Print,  spawn,          SHCMD("maimpick")}, /* opens up maimpick screenshot script */
    { 0,                            XK_Scroll_Lock, spawn,     SHCMD("emoji")}, /* launches emoji dmenu script */
	{ 0,                            XK_Pause,  spawn,          SHCMD("feh --random --bg-scale ~/.config/wallpapers/")}, /* sets random wallpapers */
	{ 0,                            XF86XK_AudioMute,          spawn, SHCMD("amixer -q set Master toggle")},     /* volume mute */
	{ 0,                            XF86XK_AudioLowerVolume,   spawn, SHCMD("amixer -q set Master 5%- unmute")}, /* volume decrease */
	{ 0,                            XF86XK_AudioRaiseVolume,   spawn, SHCMD("amixer -q set Master 5%+ unmute")}, /* volume increase */
	{ MODKEY,                       XK_b,      togglebar,      {0} },
	{ MODKEY,                       XK_o,      incnmaster,     {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_o,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,                       XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_y,      setlayout,      {.v = &layouts[3]} },
	{ MODKEY|ShiftMask,             XK_y,      setlayout,      {.v = &layouts[4]} },
    { MODKEY,                       XK_u,      setlayout,      {.v = &layouts[5]} },
    { MODKEY|ShiftMask,             XK_u,      setlayout,      {.v = &layouts[6]} },
    { MODKEY,                       XK_i,      setlayout,      {.v = &layouts[7]} },
	{ MODKEY,                       XK_s,      togglesticky,   {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ Mod1Mask,                     XK_comma,  focusmon,       {.i = -1 } },
	{ Mod1Mask,                     XK_period, focusmon,       {.i = +1 } },
	{ Mod1Mask|ShiftMask,           XK_comma,  tagmon,         {.i = -1 } },
	{ Mod1Mask|ShiftMask,           XK_period, tagmon,         {.i = +1 } },
    { MODKEY|Mod1Mask,              XK_u,      incrgaps,       {.i = 1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_u,      incrgaps,       {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_i,      incrigaps,      {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } },
	{ MODKEY,                       XK_equal,  incrogaps,      {.i = +1 } },
	{ MODKEY,                       XK_minus,  incrogaps,      {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_6,      incrihgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_7,      incrivgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_8,      incrohgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
	{ MODKEY|Mod1Mask,              XK_9,      incrovgaps,     {.i = +1 } },
	{ MODKEY|Mod1Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
	{ MODKEY|ShiftMask|ControlMask, XK_equal,  togglegaps,     {0} },
	{ MODKEY|ShiftMask,             XK_equal,  defaultgaps,    {0} },
	STACKKEYS(MODKEY,                          focus)
	STACKKEYS(MODKEY|ShiftMask,                push)
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

